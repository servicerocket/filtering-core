package org.randombits.filtering.core.criteria.general;

import org.randombits.filtering.core.criteria.Criterion;

public abstract class GeneralCriterion<T> implements Criterion {
    private boolean required;

    private Class<T> type;

    public GeneralCriterion( Class<T> type ) {
        this.type = type;
    }

    public boolean matches( Object object ) {
        boolean matches = true;

        T value = convert( object );
        if ( object == null || value != null )
            matches = matchesType( value );

        if ( matches && object == null )
            matches = !required;

        return matches;
    }

    protected T convert( Object object ) {
        if ( type.isInstance( object ) )
            return type.cast( object );
        return null;
    }

    protected abstract boolean matchesType( T object );

    public boolean isRequired() {
        return required;
    }

    public void setRequired( boolean required ) {
        this.required = required;
    }

}

/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.core.criteria.general;

import java.util.regex.Pattern;

/**
 * Compares the string value of an object to set criteria.
 * 
 * @author David Peterson
 */
public class TextCriterion extends GeneralCriterion<Object> {
    private Pattern include;

    private Pattern exclude;

    private int minLength = -1;

    private int maxLength = -1;

    private String value;

    public TextCriterion() {
        super( Object.class );
    }

    public TextCriterion( Pattern include, Pattern exclude ) {
        this( -1, -1, include, exclude );
    }

    public TextCriterion( int minLength, int maxLength ) {
        this( minLength, maxLength, null, null );
    }

    public TextCriterion( int minLength, int maxLength, Pattern include, Pattern exclude ) {
        this();
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.include = include;
        this.exclude = exclude;
    }

    /**
     * Checks if the object matches this criterion.
     * 
     * @param object
     *            The object.
     * @return <code>true</code> if the object matches this criterion.
     */
    @Override
    public boolean matchesType( Object object ) {
        String stringValue;
        stringValue = object == null ? "" : object.toString();
        stringValue = stringValue == null ? "" : stringValue;

        if ( isRequired() && stringValue.trim().length() == 0 )
            return false;

        if ( minLength >= 0 && stringValue.length() < minLength )
            return false;

        if ( maxLength >= 0 && stringValue.length() > maxLength )
            return false;

        if ( value != null && !value.equals( stringValue ) )
            return false;

        if ( include != null && !include.matcher( stringValue ).matches() )
            return false;

        if ( exclude != null && exclude.matcher( stringValue ).matches() )
            return false;

        return true;
    }

    public Pattern getInclude() {
        return include;
    }

    public Pattern getExclude() {
        return exclude;
    }

    public int getMinLength() {
        return minLength;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setInclude( Pattern include ) {
        this.include = include;
    }

    public void setExclude( Pattern exclude ) {
        this.exclude = exclude;
    }

    public void setMinLength( int minLength ) {
        this.minLength = minLength;
    }

    public void setMaxLength( int maxLength ) {
        this.maxLength = maxLength;
    }

    public String getValue() {
        return value;
    }

    public void setValue( String value ) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "{text"
                + ( minLength >= 0 ? "; min length: " + minLength : "" )
                + ( maxLength >= 0 ? "; max length: " + maxLength : "" )
                + ( include != null ? "; include: " + include : "" )
                + ( exclude != null ? "; exclude: " + exclude : "" )
                + ( value != null ? "; value: " + value : "" )
                + "}";

    }
}

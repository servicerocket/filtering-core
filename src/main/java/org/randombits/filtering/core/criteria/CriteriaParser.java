/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.core.criteria;

import org.randombits.utils.text.TextBuffer;
import org.randombits.utils.text.TokenIterator;

import java.util.BitSet;

/**
 * This class parses strings using +/- quoted form for mixed boolean filtering.
 *
 * @author David Peterson
 */
public class CriteriaParser {

    private static final char OPEN_PAREN_CHAR = '(';

    private static final char CLOSE_PAREN_CHAR = ')';

    private static final char REQ_CHAR = '+';

    private static final char EXCLUDED_CHAR = '-';

    private static final char QUOTE_CHAR = '"';

    private static final char ESCAPE_CHAR = '\\';

    private static final char COMMA_CHAR = ',';

    private static final char SEMICOLON_CHAR = ';';

    private static final BitSet WHITESPACE = new BitSet();

    private static final BitSet SEPARATOR = new BitSet();

    private static final BitSet RESERVED = new BitSet();

    private static final BitSet UNQUOTED = new BitSet();

    private static final BitSet QUOTED = new BitSet();

    static {
        WHITESPACE.set( ' ' );
        WHITESPACE.set( '\t' );
        WHITESPACE.set( '\r' );
        WHITESPACE.set( '\n' );

        SEPARATOR.set( COMMA_CHAR );
        SEPARATOR.set( SEMICOLON_CHAR );

        RESERVED.set( OPEN_PAREN_CHAR );
        RESERVED.set( CLOSE_PAREN_CHAR );
        RESERVED.set( REQ_CHAR );
        RESERVED.set( EXCLUDED_CHAR );
        RESERVED.set( QUOTE_CHAR );

        UNQUOTED.set( Character.MIN_VALUE, Character.MAX_VALUE, true );
        UNQUOTED.andNot( SEPARATOR );
        UNQUOTED.andNot( RESERVED );
        UNQUOTED.set( EXCLUDED_CHAR );

        QUOTED.set( Character.MIN_VALUE, Character.MAX_VALUE, true );
        QUOTED.clear( QUOTE_CHAR );
        QUOTED.clear( ESCAPE_CHAR );
    }

    private BitSet separator;

    private BitSet unquoted;

    public CriteriaParser() {
        this( false );
    }

    public CriteriaParser( boolean whitespaceSeparator ) {
        separator = new BitSet();
        separator.or( SEPARATOR );

        unquoted = new BitSet();
        unquoted.or( UNQUOTED );

        if ( whitespaceSeparator ) {
            separator.or( WHITESPACE );
            unquoted.andNot( WHITESPACE );
        }
    }

    /**
     * Parses the provided filter value, using the specified criterion parser to
     * findObject individual criterion.
     *
     * @param filterValue          the filter value.
     * @param criterionInterpreter The interpreter.
     * @return The parsed criterion.
     * @throws CriteriaException if there was a problem parsing the parameter.
     */
    public Criterion parse( String filterValue, CriterionInterpreter criterionInterpreter )
            throws CriteriaException {
        TokenIterator i = new TokenIterator( filterValue.trim() );
        Criterion criterion = parseList( i, criterionInterpreter );
        if ( !i.atEnd() )
            throw new CriteriaException( "Unexpected values at end: '" + i.getSequence( TokenIterator.ALL_CHARS )
                    + "'" );

        return criterion;

    }

    private Criterion parseList( TokenIterator i, CriterionInterpreter criterionInterpreter )
            throws CriteriaException {
        Criteria optCriteria = new OrCriteria();
        Criteria reqCriteria = new AndCriteria();
        Criteria excludedCriteria = new AndCriteria();
        boolean continuing = true;

        while ( continuing && !i.atEnd() ) {
            parseListItem( i, criterionInterpreter, optCriteria, reqCriteria, excludedCriteria );

            // Clear any extra whitespace...
            // i.getSequence(WHITESPACE);
            continuing = i.getSequence( separator, 1 ) != null;
        }

        if ( excludedCriteria.getCriteria().size() > 0 ) {
            Criteria criteria = new AndCriteria();
            criteria.addCriterion( excludedCriteria );

            if ( reqCriteria.getCriteria().size() > 0 )
                criteria.addCriterion( reqCriteria );
            else if ( optCriteria.getCriteria().size() > 0 )
                criteria.addCriterion( optCriteria );
            else
                return excludedCriteria;

            return criteria;
        } else if ( reqCriteria.getCriteria().size() > 0 )
            return reqCriteria;
        else
            return optCriteria;
    }

    private void parseListItem( TokenIterator i, CriterionInterpreter criterionInterpreter, Criteria optCriteria,
                                Criteria reqCriteria, Criteria excludedCriteria ) throws CriteriaException {
        // clear any whitespace
        i.getSequence( WHITESPACE );

        if ( i.getToken( REQ_CHAR ) != null ) {
            reqCriteria.addCriterion( parseCriterion( i, criterionInterpreter ) );
        } else if ( i.getToken( EXCLUDED_CHAR ) != null ) {
            excludedCriteria.addCriterion( new NotCriterion( parseCriterion( i, criterionInterpreter ) ) );
        } else {
            optCriteria.addCriterion( parseCriterion( i, criterionInterpreter ) );
        }
    }

    private Criterion parseGroup( TokenIterator i, CriterionInterpreter criterionInterpreter )
            throws CriteriaException {
        i.getToken( OPEN_PAREN_CHAR );
        Criterion group = parseList( i, criterionInterpreter );
        if ( i.getToken( CLOSE_PAREN_CHAR ) == null )
            throw new CriteriaException( "Expected '" + CLOSE_PAREN_CHAR + "'" );
        return group;
    }

    private Criterion parseCriterion( TokenIterator i, CriterionInterpreter criterionInterpreter )
            throws CriteriaException {
        if ( i.matchToken( OPEN_PAREN_CHAR ) ) {
            return parseGroup( i, criterionInterpreter );
        } else {
            TextBuffer buff = new TextBuffer();

            boolean done = false;

            while ( !done ) {
                if ( i.getToken( QUOTE_CHAR ) != null ) {
                    readQuotedCriterion( i, buff );
                } else {
                    CharSequence value = i.getSequence( unquoted, 1 );
                    if ( value != null )
                        buff.add( value );
                    else
                        done = true;
                }
            }

            return criterionInterpreter.createCriterion( buff.toString() );
        }
    }

    private void readQuotedCriterion( TokenIterator i, TextBuffer buff ) throws CriteriaException {
        while ( !i.atEnd() && !i.matchToken( QUOTE_CHAR ) ) {
            buff.add( i.getSequence( QUOTED ) );
            if ( i.getToken( ESCAPE_CHAR ) != null ) {
                CharSequence escaped = i.getSequence( TokenIterator.ALL_CHARS, 1, 1 );
                if ( escaped == null )
                    throw new CriteriaException( "Expected a character to escape after '" + ESCAPE_CHAR + "'" );

                buff.add( escaped );
            }
        }
        if ( i.getToken( QUOTE_CHAR ) == null )
            throw new CriteriaException( "Expected '" + QUOTE_CHAR + "'" );
    }
}

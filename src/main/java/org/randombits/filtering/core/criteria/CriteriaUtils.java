package org.randombits.filtering.core.criteria;

import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.iterators.FilterIterator;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class CriteriaUtils {
    public CriteriaUtils() {
    }

    /**
     * Modifies the provided collection to only include objects which match the
     * specified criterion.
     * 
     * @param <T>
     *            The collection type.
     * @param collection
     *            The collection.
     * @param criterion
     *            The criterion to filter on.
     */
    public static <T> void filterCollectionContents( Collection<T> collection, Criterion criterion ) {
        Iterator<T> i = collection.iterator();
        while ( i.hasNext() ) {
            T value = i.next();
            if ( !criterion.matches( value ) )
                i.remove();
        }
    }

    public static <T> List<T> filterIntoList( Iterable<T> iterable, Criterion criterion ) {
        return filterIntoList( iterable.iterator(), criterion );
    }

    public static <T> List<T> filterIntoList( Iterator<T> iterator, Criterion criterion ) {
        List<T> values = new java.util.ArrayList<T>();
        while ( iterator.hasNext() ) {
            T value = iterator.next();
            if ( criterion.matches( value ) )
                values.add( value );
        }
        return values;
    }

    public static <T> Iterator<T> filterIntoIterator( Iterable<T> iterable, Criterion criterion ) {
        return filterIntoIterator( iterable.iterator(), criterion );
    }

    @SuppressWarnings("unchecked")
    public static <T> Iterator<T> filterIntoIterator( Iterator<T> iterator, final Criterion criterion ) {
        return new FilterIterator( iterator, new Predicate() {
            public boolean evaluate( Object value ) {
                return criterion.matches( value );
            }
        } );
    }
}

package org.randombits.filtering.core.criteria;

public class CriteriaException extends Exception {

    public CriteriaException() {
        super();
    }

    public CriteriaException( String message, Throwable throwable ) {
        super( message, throwable );
    }

    public CriteriaException( String message ) {
        super( message );
    }

    public CriteriaException( Throwable throwable ) {
        super( throwable );
    }

}

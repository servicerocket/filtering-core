/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.core.criteria.general;

import java.math.BigDecimal;

/**
 * Checks if the object is a number which matches the specified settings.
 *
 * @author David Peterson
 */
public class NumberCriterion extends GeneralCriterion<Number> {
    private Number minValue;

    private Number maxValue;

    private Number aboveValue;

    private Number belowValue;

    private Boolean decimal;

    private Number value;

    public NumberCriterion() {
        this( null, null, null );
    }

    public NumberCriterion( Boolean isDecimal ) {
        this( null, null, isDecimal );
    }

    public NumberCriterion( Number minValue, Number maxValue ) {
        this( minValue, maxValue, null );
    }

    public NumberCriterion( Number minValue, Number maxValue, Boolean isDecimal ) {
        super( Number.class );
        this.minValue = minValue;
        this.maxValue = maxValue;

        this.decimal = isDecimal;
    }

    /**
     * Checks if the object matches this criterion.
     *
     * @param number The number to check.
     * @return <code>true</code> if the object matches this criterion.
     */
    @Override
    public boolean matchesType( Number number ) {
        if ( minValue != null && ( number == null || minValue.doubleValue() > number.doubleValue() ) )
            return false;

        if ( maxValue != null && ( number == null || maxValue.doubleValue() < number.doubleValue() ) )
            return false;

        if ( aboveValue != null && ( number == null || aboveValue.doubleValue() >= number.doubleValue() ) )
            return false;

        if ( belowValue != null && ( number == null || belowValue.doubleValue() <= number.doubleValue() ) )
            return false;

        if ( value != null && (number == null || value.doubleValue() != number.doubleValue() ) )
            return false;

        if ( decimal != null && decimal.booleanValue() != isDecimalInstance( number ) )
            return false;

        return true;
    }

    private boolean isDecimalInstance( Number number ) {
        return number instanceof Float || number instanceof Double || number instanceof BigDecimal;
    }

    public Number getMinValue() {
        return minValue;
    }

    public void setMinValue( Number minValue ) {
        this.minValue = minValue;
    }

    public Number getMaxValue() {
        return maxValue;
    }

    public void setMaxValue( Number maxValue ) {
        this.maxValue = maxValue;
    }

    /**
     * Sets whether the number may or may not be a decimal value (float,
     * double). If <code>true</code>, the value must be decimal. If
     * <code>false</code>, it may not be decimal. If <code>null</code>, it
     * can be either (the default value).
     *
     * @param isDecimal The decimal value requirement.
     */
    public void setDecimal( Boolean isDecimal ) {
        decimal = isDecimal;
    }

    public Boolean isDecimal() {
        return decimal;
    }

    public Number getValue() {
        return value;
    }

    public void setValue( Number value ) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "{number"
                + ( minValue != null ? "; min: " + minValue : "" )
                + ( maxValue != null ? "; max: " + maxValue : "" )
                + ( aboveValue != null ? "; above: " + aboveValue : "" )
                + ( belowValue != null ? "; below: " + belowValue : "" )
                + ( decimal != null ? "; is decimal: " + decimal : "" ) + "}";
    }

    public Number getAboveValue() {
        return aboveValue;
    }

    public void setAboveValue( Number aboveValue ) {
        this.aboveValue = aboveValue;
    }

    public Number getBelowValue() {
        return belowValue;
    }

    public void setBelowValue( Number belowValue ) {
        this.belowValue = belowValue;
    }
}

package org.randombits.filtering.core.criteria;

import java.util.Collection;

/**
 * Objects implementing this method are a source for objects.
 * 
 * @author David Peterson
 */
public interface SourceCriterion<T> extends Criterion {

    public enum Weight {
        LIGHT, MEDIUM, HEAVY;
    }

    /**
     * Returns the set of values that match this criterion.
     * 
     * @return
     */
    public Collection<T> getMatchingValues();

    /**
     * Returns a hint about the general amount of processing typically required
     * for finding matching values. This allows lighter-weight criteria to be
     * processed first, potentially skipping heaver criteria altogether if the
     * result set can be determined earlier.
     * 
     * @return the weight for the criterion.
     */
    public Weight getWeight();
}

package org.randombits.filtering.core.criteria;

import java.util.Comparator;

/**
 * Compares two source criterion by their weight, putting lighter weight
 * criterion higher up the order.
 * 
 * @author David Peterson
 * 
 */
public class SourceCriterionComparator implements Comparator<SourceCriterion<?>> {

    public int compare( SourceCriterion<?> o1, SourceCriterion<?> o2 ) {
        return o1.getWeight().compareTo( o2.getWeight() );
    }
}

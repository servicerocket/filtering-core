/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.core.criteria.general;

import java.util.Date;

/**
 * Checks if the specified object is a {@link java.util.Date} and that it
 * matches the specified settings.
 *
 * @author David Peterson
 */
public class DateCriterion extends GeneralCriterion<Date> {
    private Date minValue;

    private Date maxValue;

    private Date afterValue;

    private Date beforeValue;

    private Date value;

    public DateCriterion() {
        super( Date.class );
    }

    public DateCriterion( Date minValue, Date maxValue ) {
        this();
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    /**
     * Checks if the object matches this criterion.
     *
     * @param date The date object.
     * @return <code>true</code> if the object matches this criterion.
     */
    @Override
    protected boolean matchesType( Date date ) {
        if ( date == null )
            return true;

        if ( minValue != null && minValue.after( date ) )
            return false;

        if ( maxValue != null && maxValue.before( date ) )
            return false;

        if ( afterValue != null && afterValue.compareTo( date ) >= 0 )
            return false;

        if ( beforeValue != null && beforeValue.compareTo( date ) <= 0 )
            return false;

        if ( value != null && !value.equals( date ) )
            return false;

        return true;
    }

    /**
     * @return The minimum value that will match (inclusive).
     */
    public Date getMinValue() {
        return minValue;
    }

    /**
     * @param minValue The minimum value that will match (inclusive).
     */
    public void setMinValue( Date minValue ) {
        this.minValue = minValue;
    }

    /**
     * @return The maximum value that will match (inclusive)
     */
    public Date getMaxValue() {
        return maxValue;
    }

    /**
     * @param maxValue The maximum value that will match (inclusive)
     */
    public void setMaxValue( Date maxValue ) {
        this.maxValue = maxValue;
    }

    /**
     * @return The minimum value that will match (exclusive)
     */
    public Date getAfterValue() {
        return afterValue;
    }

    /**
     * @param afterValue The minimum value that will match (exclusive)
     */
    public void setAfterValue( Date afterValue ) {
        this.afterValue = afterValue;
    }

    /**
     * @return The maximum value that will match (exclusive)
     */
    public Date getBeforeValue() {
        return beforeValue;
    }

    /**
     * @param beforeValue The maximum value that will match (exclusive)
     */
    public void setBeforeValue( Date beforeValue ) {
        this.beforeValue = beforeValue;
    }

    /**
     * @return The exact value to match, to the millisecond.
     */
    public Date getValue() {
        return value;
    }

    /**
     * @param value The exact value to match, to the millisecond.
     */
    public void setValue( Date value ) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "{date"
                + ( minValue != null ? "; min: " + minValue : "" )
                + ( maxValue != null ? "; max: " + maxValue : "" )
                + ( afterValue != null ? "; after: " + afterValue : "" )
                + ( beforeValue != null ? "; before: " + beforeValue : "" )
                + ( value != null ? "; value: " + value : "" )
                + "}";
    }
}

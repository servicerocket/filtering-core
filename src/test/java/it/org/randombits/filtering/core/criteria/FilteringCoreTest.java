package it.org.randombits.filtering.core.criteria;

import com.atlassian.confluence.it.User;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import it.com.servicerocket.randombits.AddOnTest;
import it.com.servicerocket.randombits.pageobject.AddOnOSGIPage;
import org.junit.Test;

/**
 * @author Kai Fung
 * @since 1.0.3.20140915
 */
public class FilteringCoreTest extends AbstractWebDriverTest {

    @Test public void testFilteringCorePluginIsInstalled() throws Exception {
        assert new AddOnTest().isInstalled(
                product.login(User.ADMIN, AddOnOSGIPage.class),
                "RB Filtering - Core"
        );
    }
}
